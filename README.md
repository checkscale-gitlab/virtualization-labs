# Laboratorios de Virtualización

Los documentos escritos en este repositorio están publicados en [rpubs.com](http://rpubs.com/osmanirosado/).

## Laboratorio 1

El objetivo de este laboratorio es crear una máquina virtual (MV) para instalar Ubuntu Server 18.04. También se debe aprender a:

- encender y apagar una MV
- exportar e importar una MV 
- crear puntos de restauración de una MV mediante snapshots

## Laboratorio 2

El objetivo de este laboratorio es instalar y configurar el sistema operativo Ubuntu Server 18.04 en la MV creada en el laboratorio 1. También debe aprender a:

- instalar programas desde los repositorios de Ubuntu
- ejecutar comandos desde una terminal o cliente ssh conectado a la MV de Ubuntu
- copiar archivos hacia y desde la MV de Ubuntu usando un cliente sftp

## Laboratorio 3

El objetivo de este laboratorio es instalar y configurar Docker. También debe aprender a:

- descargar una imágen de Docker
- crear y ejecutar un contenedor a partir de una imágen de Docker
- crear imágenes de Docker a partir de un Dockerfile

## Laboratorio 4

El objetivo de este laboratorio es desarrollar habilidades básicas para el trabajo con usuarios y permisos en los sistemas operativos GNU/Linux.

## Ejemplos

Algunos ejemplos de Docker que debe estudiar antes de hacer el laboratorio 3: Primeros pasos con Docker.

## RStudio

Los documentos han sido escritos usando el lenguaje R Markdown. Para editar estos documentos se puede usar el RStudio.

El RStudio se puede usar en la [nube](https://rstudio.cloud) o localmente.
Existen al menos dos opciones para instalarlo localmente:
usar un contenedor de docker como [rocker/verse](https://hub.docker.com/r/rocker/verse) o
instalar el [programa](https://www.rstudio.com/products/rstudio/download/) directamente en el sistema.
